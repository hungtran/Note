<div id="map" style="width: 500px; height: 400px;"></div>
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyTM2ZJEO1GIpVVS_gZzg0uIJ_g8osu-0">
</script>
<script type="text/javascript">
    var locations = [
        ['50 Nguy?n Cu Trinh, Q.1, TP. H? Chí Minh','10.764641','106.691396'],
        ['240 Nguy?n Cu Trinh, Q.1, TP. H? Chí Minh','10.762533','106.686640'],
        ['790 Nguy?n Cu Trinh, Q.1, TP. H? Chí Minh','10.764113','106.689420'],
        ['80 Nguy?n Cu Trinh, Q.1, TP. H? Chí Minh','10.764426','106.690555'],
        ['120 Nguy?n Cu Trinh, Q.1, TP. H? Chí Minh','10.761951','106.687000']
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(10.764113, 106.689420),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
</script>